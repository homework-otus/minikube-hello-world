```shell script
# Frequent commands
minikube start --cpus=8 --vm-driver='virtualbox'
minikube status
minikube addons enable ingress
minikube dashboard
kubectl apply -f .
kubectl get deployments
kubectl get services
kubectl get pods
kubectl get ing
kubectl get all
kubectl delete all --all
minikube delete
minikube ip

kubectl config set-context --current --namespace=myapp
watch -n 2 "kubectl get all"
```

```shell script
# Homework check (ip need get from ingress)
curl -H 'Host: arch.homework' http://192.168.99.106/otusapp/health
```

https://kubernetes.io/ru/docs/tasks/tools/install-minikube/
https://kubernetes.io/ru/docs/tutorials/hello-minikube/